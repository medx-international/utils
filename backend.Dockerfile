FROM node:hydrogen

RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add - && \
    echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.2 main" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list && \
    apt-get update && \
    apt-get install gnupg mongodb-org-tools wait-for-it default-jre -y --allow-unauthenticated && \
    apt-get clean && rm -rf /var/lib/apt/lists/*