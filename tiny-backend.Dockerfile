FROM node:hydrogen-alpine

RUN apk add mongodb-tools bash openjdk11-jre curl

WORKDIR /app
