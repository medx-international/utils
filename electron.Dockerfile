FROM node:hydrogen

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Africa/Kampala

RUN dpkg --add-architecture i386 \
    && apt-get update \
    && apt-get install -y git curl gnupg unixodbc-dev \
    unixodbc-dev:i386 libodbc1:i386 odbcinst1debian2:i386 \
    build-essential python3 mono-devel wine64 \
    ca-certificates-mono gcc-multilib g++-multilib \
    && apt-get clean && rm -rf /var/lib/apt/lists/*